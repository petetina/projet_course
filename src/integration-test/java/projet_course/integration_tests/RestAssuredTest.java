package projet_course.integration_tests;

import io.restassured.RestAssured;
import org.junit.BeforeClass;

public class RestAssuredTest {

    @BeforeClass
    public static void setUp() {

        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
            //RestAssured.port = Integer.valueOf(9135);
        } else {
            RestAssured.port = Integer.valueOf(port);
        }

        String basePath = System.getProperty("server.base");
        if (basePath == null) {
            basePath = "/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if (baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

    }

}
