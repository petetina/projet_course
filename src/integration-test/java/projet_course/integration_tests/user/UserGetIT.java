package projet_course.integration_tests.user;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;

public class UserGetIT extends RestAssuredTest {

    /* Positive tests */

    @Test
    public void should_log_in_on_valid_email_and_password() {

        given()
                .formParam("mail", "a.b@gmail.com")
                .formParam("password", 123456)
                .when()
                .get("/users/login")
                .then()
                .statusCode(SC_OK);

    }



    /* Negative tests */
    @Test
    public void should_not_log_in_on_bad_email() {

        given()
                .formParam("mail", "azerty@gmail.com")
                .formParam("password", "nevermind")
                .when()
                .get("/users/login")
                .then()
                .statusCode(SC_NOT_FOUND);

    }

    @Test
    public void should_not_log_in_on_bad_password_but_good_email() {

        given()
                .formParam("mail", "a.b@gmail.com")
                .formParam("password", "mywrongpassword")
                .when()
                .get("/users/login")
                .then()
                .statusCode(SC_NOT_FOUND);

    }

}

