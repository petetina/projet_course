package projet_course.integration_tests.fridge;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;

public class FridgePostIT extends RestAssuredTest {

    /* Positive test*/

    @Test
    public void should_add_component_on_existing_fridge_when_product_is_not_already_in_fridge() {

        given()
                .formParam("ingredient_name","Comté")
                .formParam("quantity",300)
                .pathParam("iduser",2)
                .post("/fridges/{iduser}")
                .then()
                .statusCode(SC_CREATED);
    }

    /* Negative tests */
    //Negative quantity
    @Test
    public void should_not_add_component_on_existing_fridge_when_product_is_not_already_in_fridge_but_quantity_in_negative() {

        given()
                .formParam("ingredient_name","Comté")
                .formParam("quantity",-2)
                .pathParam("iduser",2)
                .post("/fridges/{iduser}")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    //product already existing
    @Test
    public void should_not_add_component_on_existing_fridge_when_product_is_already_in_fridge() {

        given()
                .formParam("ingredient_name","Oeuf")
                .formParam("quantity",6)
                .pathParam("iduser",2)
                .post("/fridges/{iduser}")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }
    //Unknown user
    @Test
    public void should_not_add_component_on_non_existing_fridge() {

        given()
                .formParam("ingredient_name","Comté")
                .formParam("quantity",300)
                .pathParam("iduser",99)
                .post("/fridges/{iduser}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }
}

