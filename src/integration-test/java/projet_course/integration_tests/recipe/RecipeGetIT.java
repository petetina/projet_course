package projet_course.integration_tests.recipe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import projet_course.domains.models.Component;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.Unit;
import projet_course.integration_tests.RestAssuredTest;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;

public class RecipeGetIT extends RestAssuredTest {

    /* Positive test */
    @Test
    public void should_user_1_able_to_do_only_recipes_2_and_4() {

        given()
                .formParam("iduser",1)
                .when()
                .get("/recipes")
                .then()
                .statusCode(SC_OK)
                .body("[0].possible",equalTo(false))
                .body("[1].possible",equalTo(true))
                .body("[2].possible",equalTo(false))
                .body("[3].possible",equalTo(true));

    }

    @Test
    public void should_user_2_able_to_do_only_recipes_3_and_4() {

        given()
                .formParam("iduser",2)
                .when()
                .get("/recipes")
                .then()
                .statusCode(SC_OK)
                .body("[0].possible",equalTo(false))
                .body("[1].possible",equalTo(false))
                .body("[2].possible",equalTo(true))
                .body("[3].possible",equalTo(true));

    }

    //Le findById
    //This recipe with this iduser allow us to test missing ingredients.
    //Missing ingredient because we don't have the ingredient in the fridge,
    //and missing ingredient because we don't have enough quantity rather than the recipe.
    @Test
    public void should_find_recipe_by_id_with_missing_components() {

        List<Component> missingComponents = new ArrayList<>();

        Component c1 = new Component();
        c1.setIngredient(new Ingredient(5,"Yahourt nature", Unit.NO_UNIT));
        c1.setQuantity(1);
        missingComponents.add(c1);

        Component c2 = new Component();
        c2.setIngredient(new Ingredient(6, "Sucre", Unit.GRAMME));
        c2.setQuantity(100);
        missingComponents.add(c2);

        Component c3 = new Component();
        c3.setIngredient(new Ingredient(8,"Huile de tournesol", Unit.ML));
        c3.setQuantity(500);
        missingComponents.add(c3);

        Component c4 = new Component();
        c4.setIngredient(new Ingredient(1, "Oeuf", Unit.NO_UNIT));
        c4.setQuantity(1);
        missingComponents.add(c4);

        Component c5 = new Component();
        c5.setIngredient(new Ingredient(9, "Levure chimique", Unit.GRAMME));
        c5.setQuantity(20);
        missingComponents.add(c5);

        ObjectMapper mapper = new ObjectMapper();
        try {
            String expected = mapper.writeValueAsString(missingComponents);
            String actual = mapper.writeValueAsString(
                    given()
                    .formParam("iduser",1)
                    .pathParam("idrecipe",3)
                    .when()
                    .get("/recipes/{idrecipe}")
                    .path("missingComponents"));
            assertJsonEquals(actual, expected);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }



    }

    /* Negative test */
    @Test
    public void should_nobody_can_do_the_first_recipe() {

        given()
                .formParam("iduser",1)
                .when()
                .get("/recipes")
                .then()
                .statusCode(SC_OK)
                .body("[0].possible",equalTo(false));

        given()
                .formParam("iduser",2)
                .when()
                .get("/recipes")
                .then()
                .statusCode(SC_OK)
                .body("[0].possible",equalTo(false));

    }


}

