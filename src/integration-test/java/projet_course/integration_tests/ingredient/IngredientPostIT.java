package projet_course.integration_tests.ingredient;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;

public class IngredientPostIT extends RestAssuredTest {

    /* Positive test*/

    @Test
    public void should_create_ingredient_which_does_not_already_exist() {

        given()
                .formParam("name","Mayonnaise")
                .formParam("unit","ML")
                .post("/ingredients")
                .then()
                .statusCode(SC_CREATED);
    }

    /* Negative tests */

    @Test
    public void should_not_create_ingredient_which_does_not_already_exist_and_name_must_be_singular() {

        given()
                .formParam("name","Poirots")
                .formParam("unit","NO_UNIT")
                .post("/ingredients")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_not_create_ingredient_with_an_existing_name() {

        given()
                .formParam("name","Lait")
                .formParam("unit","ML")
                .post("/ingredients")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_not_create_ingredient_with_an_existing_name_with_case() {

        given()
                .formParam("name","lait")
                .formParam("unit","ML")
                .post("/ingredients")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_not_create_ingredient_with_an_existing_name_and_a_different_unit() {

        given()
                .formParam("name","Lait")
                .formParam("unit","GRAMME")
                .post("/ingredients")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_not_create_ingredient_with_an_unknown_unit() {

        given()
                .formParam("name","Kiwi")
                .formParam("unit","blabla")
                .post("/ingredients")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

}

