package projet_course.domains.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="recipe_seq_gen")
    @SequenceGenerator(name="recipe_seq_gen", sequenceName="RECIPE_SEQ")
    private int id;

    private int nbPeople;

    private String title;

    @OneToMany(mappedBy = "recipe",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Component> components;

    @Transient
    private List<Component> missingComponents;

    @Column( length = 100000 )
    private String description;

    //This field is modified dynamically according to a projet_course.integration_tests.fridge !
    //I want to display it, so i've had the JsonInclude Annotation,
    //but I don't want hibernate to store, so i've used transient annotation ;)
    @JsonInclude()
    @Transient
    private boolean possible;

    public Recipe(){
        this.components = new ArrayList<>();
        this.missingComponents = new ArrayList<>();
        this.title = "";
        this.description = "";
        this.nbPeople = 1;
        this.possible = false;
    }

    public Recipe(String title){
        this.components = new ArrayList<>();
        this.missingComponents = new ArrayList<>();
        this.title = StringUtils.capitalize(title.trim());
        this.description = "";
        this.nbPeople = 1;
        this.possible = false;
    }

    public Recipe(int id){
        this.id = id;
        this.title = "";
        this.components = new ArrayList<>();
        this.missingComponents = new ArrayList<>();
        this.description = "";
        this.nbPeople = 1;
        this.possible = false;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getNbPeople() {
        return nbPeople;
    }

    public void setNbPeople(int nbPeople) {
        this.nbPeople = nbPeople;
    }

    public void add(Component component){
        components.add(component);
        component.setRecipe(this);
    }

    public void addMissingComponent(Component component){
        missingComponents.add(component);
    }

    public List<Component> getComponents() {
        return components;
    }

    public List<Component> getMissingComponents() {
        return missingComponents;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPossible() {
        return missingComponents.isEmpty();
    }

    private static boolean isValid(String name){
        return !name.trim().isEmpty() && name.replace(" ","").chars().allMatch(Character::isLetter);
    }

    @JsonIgnore
    public boolean isValid(){
        return isValid(title) && nbPeople > 0 && !description.trim().isEmpty() && !components.isEmpty();
    }

}

