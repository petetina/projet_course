package projet_course.domains.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Component {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="component_seq_gen")
    @SequenceGenerator(name="component_seq_gen", sequenceName="COMPONENT_SEQ")
    private int id;

    private double quantity;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name="ingredient_id")
    private Ingredient ingredient;

    //Allow to remove ingredient and component in cascade, component need to know fridge
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "frigde_id")
    private Fridge fridge;

    @ManyToOne
    private Recipe recipe;

    public Component(){
        this.quantity = 0;
        this.ingredient = null;
        this.recipe = null;
    }

    public Component(int id){
        this.id = id;
        this.quantity = 0;
        this.ingredient = null;
        this.recipe = null;
    }

    public int getId() {
        return id;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
        if(ingredient != null)
            ingredient.linkWithComponent(this);
    }

    public double getQuantity() {
        return quantity;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public Fridge getFridge() {
        return fridge;
    }

    public void setFridge(Fridge fridge) {
        this.fridge = fridge;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
