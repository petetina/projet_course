package projet_course.domains.services;

import org.springframework.stereotype.Service;
import projet_course.domains.models.Component;
import projet_course.domains.models.Fridge;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.User;
import projet_course.repositories.ComponentRepository;
import projet_course.repositories.FridgeRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class FridgeService {
    private ComponentRepository componentRepository;
    private FridgeRepository fridgeRepository;
    private UserService userService;
    private IngredientService ingredientService;

    public FridgeService(ComponentRepository componentRepository, UserService userService, IngredientService ingredientService, FridgeRepository fridgeRepository) {
        this.componentRepository = componentRepository;
        this.fridgeRepository = fridgeRepository;
        this.userService = userService;
        this.ingredientService = ingredientService;

    }

    public List<Fridge> all() {
        return fridgeRepository.findAll();
    }

    public Fridge findById(int id){

        List<Fridge> results = fridgeRepository.findAll().stream().filter(f -> f.getId()==id).collect(toList());
        if(!results.isEmpty())
            return results.get(0);
        else
            return null;
    }

    private Component findComponentInFridge(int fridgeId, String ingredientName){
        Component component = null;

        Fridge fridge = fridgeRepository.findOne(fridgeId);
        if(fridge!=null)
        {
            List<Component> components = fridge.getComponents().stream().filter(c -> c.getIngredient().getName().equalsIgnoreCase(ingredientName)).collect(toList());
            if(!components.isEmpty())
                component = components.get(0);
        }
        return component;
    }

    private Component findComponentInFridge(int fridgeId, int componentId){
        Component component = null;

        Fridge fridge = fridgeRepository.findOne(fridgeId);
        if(fridge!=null)
        {
            List<Component> components = fridge.getComponents().stream().filter(c -> c.getId() == componentId).collect(toList());
            if(!components.isEmpty())
                component = components.get(0);
        }
        return component;
    }

    public int addComponent(String ingredientName, double quantity, int idUser){
        //First, we check if the projet_course.integration_tests.user exists

        if(quantity <= 0)
            return -3;

        User user = userService.findById(idUser);
        if(user == null) {
            return -2;
        }

        Ingredient ingredient = ingredientService.findByName(ingredientName);

        if(findComponentInFridge(user.getFridge().getId(),ingredientName) != null){
            return -1;
        }

        Component component = new Component();
        component.setIngredient(ingredient);
        component.setQuantity(quantity);

        return componentRepository.save(component).getId();
    }


    public int updateQuantity(int idUser, int idComponent, double quantityToAddOrRemove) {

        Component component = componentRepository.findOne(idComponent);
        if (component!=null) {
            //To be safe, we must verify that the modified component is linked with the user's fridge
            User user = userService.findById(idUser);
            if(user == null)
                return -1;

            if(findComponentInFridge(user.getFridge().getId(),idComponent) == null)
                return -2;


            double leftSize = component.getQuantity() + quantityToAddOrRemove;

            if( leftSize > 0){
                component.setQuantity(component.getQuantity() + quantityToAddOrRemove);
                Component newComponent = componentRepository.save(component);
                if(newComponent != null)
                    return newComponent.getId();
                else
                    return -2;
            }else if(leftSize == 0) {
                if(deleteComponent(idUser,idComponent))
                    return idComponent;//Return a positive number
                else
                    return -3;
            }else{
                return -2;
            }

        } else {
            return -1;
        }
    }

    public boolean deleteComponent(int idUser, int idComponent){
        Component component = componentRepository.findOne(idComponent);
        if (component!=null) {

            //To be safe, we must verify that the modified component is linked with the projet_course.integration_tests.user's projet_course.integration_tests.fridge
            User user = userService.findById(idUser);
            if(user == null)
                return false;

            if(findComponentInFridge(user.getFridge().getId(),component.getId()) == null)
                return false;
            int oldSize = componentRepository.findAll().size();
            component.setFridge(null);
            component.setIngredient(null);
            componentRepository.save(component);
            componentRepository.delete(idComponent);
            int newSize = componentRepository.findAll().size();
            return newSize < oldSize;
        } else
            return false;

    }

}

