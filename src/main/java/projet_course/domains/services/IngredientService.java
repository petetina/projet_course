package projet_course.domains.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import projet_course.controllers.rest.RecipeController;
import projet_course.domains.models.Component;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.Unit;
import projet_course.repositories.ComponentRepository;
import projet_course.repositories.IngredientRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class IngredientService {
    private IngredientRepository ingredientRepository;
    private ComponentRepository componentRepository;
    private UnitService unitService;

    static Logger log = Logger.getLogger(RecipeController.class.getName());

    public IngredientService(IngredientRepository ingredientRepository, ComponentRepository componentRepository, UnitService unitService) {
        this.ingredientRepository = ingredientRepository;
        this.unitService = unitService;
        this.componentRepository = componentRepository;
    }

    public boolean isNameValid(String name){
        return !name.trim().isEmpty() && (name.charAt(name.length()-1) != 's');

    }

    public List<Ingredient> all() {
        return ingredientRepository.findAll();
    }

    public Ingredient findByName(String name){

        List<Ingredient> results = ingredientRepository.findAll().stream().filter(u -> u.getName().equalsIgnoreCase(name)).collect(toList());
        if(!results.isEmpty() && isNameValid(name))
            return results.get(0);
        else
            return null;
    }

    public List<Ingredient> findAllByName(String name){

        return ingredientRepository.findAll().stream().filter(u -> u.getName().toLowerCase().contains(name.trim().toLowerCase())).collect(toList());
    }

    public int create(String name, String unitName){
        int id = -1;
        //To catch valueOf exception
        try {
            if (isNameValid(name) && unitService.exists(unitName) && findByName(name) == null) {
                Ingredient ingredient = ingredientRepository.save(new Ingredient(name, Unit.valueOf(unitName)));
                id = ingredient.getId();
            }
        }catch (Exception e){
            log.debug(e.getMessage());
        }
        return id;


    }

    public Ingredient findById(int idingredient) {
        List<Ingredient> results = ingredientRepository.findAll().stream().filter(u -> u.getId() ==  idingredient).collect(toList());
        if(!results.isEmpty())
            return results.get(0);
        else
            return null;
    }

    public boolean exists(int idingredient) {
        List<Ingredient> results = ingredientRepository.findAll().stream().filter(u -> u.getId() ==  idingredient).collect(toList());
        return !results.isEmpty();
    }

    public boolean deleteById(int idingredient) {
        Ingredient ingredient = ingredientRepository.findOne(idingredient);
        if(ingredient != null) {
            log.debug("DELETE !!!!!!!!!!!!!!!!!!!!!!!!");
            for(Component c : ingredient.getLinkedComponents())
                componentRepository.delete(c.getId());
        }
        return true;
    }


}

