package projet_course.domains.services;

import org.springframework.stereotype.Service;
import projet_course.domains.models.*;
import projet_course.repositories.*;

import static projet_course.domains.models.Unit.*;

@Service
public class GoldenDataService {

    private UserRepository userRepository;
    private IngredientRepository ingredientRepository;
    private RecipeRepository recipeRepository;
    private boolean initialized = false;

    public GoldenDataService(UserRepository userRepository, IngredientRepository ingredientRepository, RecipeRepository recipeRepository){
        this.userRepository = userRepository;
        this.ingredientRepository = ingredientRepository;
        this.recipeRepository = recipeRepository;
    }

    public void initialize() {

        if (!initialized){

            insertIngredients();
            insertUsersWithTheirsFridges();
            insertRecipes();

            //To avoid data to stored a second time
            initialized = true;
        }

    }

    private void insertIngredients(){

        //ingredients eventually linked with a least one projet_course.integration_tests.recipe
        //1
        ingredientRepository.save(new Ingredient("Oeuf", NO_UNIT));
        //2
        ingredientRepository.save(new Ingredient("Beurre", GRAMME));
        //3
        ingredientRepository.save(new Ingredient("Chocolat noir",GRAMME));
        //4
        ingredientRepository.save(new Ingredient("Sucre vanillé",GRAMME));
        //5
        ingredientRepository.save(new Ingredient("Yahourt nature",NO_UNIT));
        //6
        ingredientRepository.save(new Ingredient("Sucre",GRAMME));
        //7
        ingredientRepository.save(new Ingredient("Farine",GRAMME));
        //8
        ingredientRepository.save(new Ingredient("Huile de tournesol",ML));
        //9
        ingredientRepository.save(new Ingredient("Levure chimique",GRAMME));
        //10
        ingredientRepository.save(new Ingredient("Pomme",NO_UNIT));
        //11
        ingredientRepository.save(new Ingredient("Cassonade",GRAMME));

        //ingredients only for fridges, no contained in any recipes
        //12
        ingredientRepository.save(new Ingredient("Lait", ML));
        //13
        ingredientRepository.save(new Ingredient("Yahourt à la fraise", NO_UNIT));
        //14
        ingredientRepository.save(new Ingredient("Coca Cola", ML));
        //15
        ingredientRepository.save(new Ingredient("Comté", GRAMME));

        //This ingredient is used by the first recipe, but nobody has carotts
        //16
        ingredientRepository.save(new Ingredient("Carotte", GRAMME));

    }

    private void insertUsersWithTheirsFridges(){
        //Create user1
        User u1 = new User("Antoine", "Petetin","a.b@gmail.com","123456");

        //Create an empty fridge
        Fridge f1 = new Fridge();

        //We save component before adding it to the fridge
        Component c11 = new Component();
        c11.setIngredient(ingredientRepository.findOne(1));
        c11.setQuantity(2);
        f1.add(c11);

        Component c12 = new Component();
        c12.setIngredient(ingredientRepository.findOne(3));
        c12.setQuantity(500);
        f1.add(c12);

        Component c13 = new Component();
        c13.setIngredient(ingredientRepository.findOne(4));
        c13.setQuantity(500);
        f1.add(c13);

        Component c14 = new Component();
        c14.setIngredient(ingredientRepository.findOne(10));
        c14.setQuantity(5);
        f1.add(c14);

        Component c15 = new Component();
        c15.setIngredient(ingredientRepository.findOne(11));
        c15.setQuantity(164);
        f1.add(c15);

        Component c16 = new Component();
        c16.setIngredient(ingredientRepository.findOne(7));
        c16.setQuantity(175);
        f1.add(c16);

        Component c17 = new Component();
        c17.setIngredient(ingredientRepository.findOne(2));
        c17.setQuantity(100);
        f1.add(c17);

        Component c18 = new Component();
        c18.setIngredient(ingredientRepository.findOne(12));
        c18.setQuantity(1000);
        f1.add(c18);

        Component c19 = new Component();
        c19.setIngredient(ingredientRepository.findOne(14));
        c19.setQuantity(500);
        f1.add(c19);

        //Bind the fridge 1 with user 1
        u1.setFridge(f1);
        u1.cryptPassword();
        //Save user1
        userRepository.save(u1);

        //Same process
        User u2 = new User("Axel", "Brogna","b.c@gmail.com","345678");

        Fridge f2 = new Fridge();

        Component c21 = new Component();
        c21.setIngredient(ingredientRepository.findOne(9));
        c21.setQuantity(50);
        f2.add(c21);

        Component c22 = new Component();
        c22.setIngredient(ingredientRepository.findOne(1));
        c22.setQuantity(4);
        f2.add(c22);

        Component c23 = new Component();
        c23.setIngredient(ingredientRepository.findOne(5));
        c23.setQuantity(5);
        f2.add(c23);

        Component c24 = new Component();
        c24.setIngredient(ingredientRepository.findOne(4));
        c24.setQuantity(150);
        f2.add(c24);

        Component c25 = new Component();
        c25.setIngredient(ingredientRepository.findOne(6));
        c25.setQuantity(200);
        f2.add(c25);

        Component c26 = new Component();
        c26.setIngredient(ingredientRepository.findOne(7));
        c26.setQuantity(352);
        f2.add(c26);

        Component c27 = new Component();
        c27.setIngredient(ingredientRepository.findOne(8));
        c27.setQuantity(1000);
        f2.add(c27);

        Component c28 = new Component();
        c28.setIngredient(ingredientRepository.findOne(2));
        c28.setQuantity(250);
        f2.add(c28);

        Component c29 = new Component();
        c29.setIngredient(ingredientRepository.findOne(10));
        c29.setQuantity(4);
        f2.add(c29);

        Component c30 = new Component();
        c30.setIngredient(ingredientRepository.findOne(11));
        c30.setQuantity(500);
        f2.add(c30);

        u2.setFridge(f2);
        u2.cryptPassword();
        userRepository.save(u2);
    }

    private void insertRecipes(){

        //Nobody has enough ingredients to do this projet_course.integration_tests.recipe
        Recipe recipe1 = new Recipe("Carottes râpées");
        recipe1.setNbPeople(1);

        Component component11 = new Component();
        component11.setIngredient(ingredientRepository.findOne(16));
        component11.setQuantity(100);
        recipe1.add(component11);

        recipe1.setDescription("Epeluchez les carottes, rapez-les, et miam !");
        recipeRepository.save(recipe1);

        //User 1 can do this projet_course.integration_tests.recipe, but not projet_course.integration_tests.user 2
        Recipe recipe2 = new Recipe("Mousse au chocolat");
        recipe2.setNbPeople(2);

        Component component21 = new Component();
        component21.setIngredient(ingredientRepository.findOne(1));
        component21.setQuantity(2);
        recipe2.add(component21);

        Component component22 = new Component();
        component22.setIngredient(ingredientRepository.findOne(3));
        component22.setQuantity(50);
        recipe2.add(component22);

        Component component23 = new Component();
        component23.setIngredient(ingredientRepository.findOne(4));
        component23.setQuantity(20);
        recipe2.add(component23);

        recipe2.setDescription(
                "    Etape 1\n" +
                "    Séparer les blancs des jaunes d'oeufs.\n" +
                "    Etape 2\n" +
                "    Faire ramollir le chocolat dans une casserole au bain-marie.\n" +
                "    Etape 3\n" +
                "    Hors du feu, incorporer les jaunes et le sucre.\n" +
                "    Etape 4\n" +
                "    Battre les blancs en neige ferme\n" +
                "    Etape 5\n" +
                "    et les ajouter délicatement au mélange à l'aide d'une spatule.\n" +
                "    Etape 6\n" +
                "    Verser dans une terrine ou des verrines\t\n" +
                "    Etape 7\n" +
                "    et mettre au frais 1 heure ou 2 minimum.\n" +
                "    Etape 8\n" +
                "    Décorer de cacao ou de chocolat râpé\n" +
                "    Etape 9\n" +
                "    Déguster\n");
        recipeRepository.save(recipe2);

        //User 2 can do this projet_course.integration_tests.recipe, but not user1
        Recipe recipe3 = new Recipe("Gâteau au yahourt");
        recipe3.setNbPeople(6);

        Component component31 = new Component();
        component31.setIngredient(ingredientRepository.findOne(5));
        component31.setQuantity(1);
        recipe3.add(component31);

        Component component32 = new Component();
        component32.setIngredient(ingredientRepository.findOne(6));
        component32.setQuantity(100);
        recipe3.add(component32);

        Component component33 = new Component();
        component33.setIngredient(ingredientRepository.findOne(4));
        component33.setQuantity(50);
        recipe3.add(component33);

        Component component34 = new Component();
        component34.setIngredient(ingredientRepository.findOne(7));
        component34.setQuantity(150);
        recipe3.add(component34);

        Component component35 = new Component();
        component35.setIngredient(ingredientRepository.findOne(8));
        component35.setQuantity(500);
        recipe3.add(component35);

        Component component36 = new Component();
        component36.setIngredient(ingredientRepository.findOne(1));
        component36.setQuantity(3);
        recipe3.add(component36);

        Component component37 = new Component();
        component37.setIngredient(ingredientRepository.findOne(9));
        component37.setQuantity(20);
        recipe3.add(component37);

        recipe3.setDescription("\n" +
                "Étape 1 :\n" +
                "\n" +
                "Allumer le four thermostat 6 (180°C). Beurrer un moule rond.\n" +
                "Étape 2 :\n" +
                "\n" +
                "Verser le yaourt dans un saladier et ajouter dans l'ordre en mélangeant bien avec une cuillère en bois : les sucres, les œufs un à un, la farine, la levure, le sel, l'huile.\n" +
                "Étape 3 :\n" +
                "\n" +
                "Mettre la préparation dans le moule beurré et faire cuire 35 minutes. Laisser refroidir et démouler.\n");
        recipeRepository.save(recipe3);

        //User 1 and user 2 can do this recipe
        Recipe recipe4 = new Recipe("Crumble aux pommes");
        recipe4.setNbPeople(4);

        Component component41 = new Component();
        component41.setIngredient(ingredientRepository.findOne(10));
        component41.setQuantity(4);
        recipe4.add(component41);

        Component component42 = new Component();
        component42.setIngredient(ingredientRepository.findOne(11));
        component42.setQuantity(150);
        recipe4.add(component42);

        Component component43 = new Component();
        component43.setIngredient(ingredientRepository.findOne(7));
        component43.setQuantity(150);
        recipe4.add(component43);

        Component component44 = new Component();
        component44.setIngredient(ingredientRepository.findOne(2));
        component44.setQuantity(90);
        recipe4.add(component44);

        recipe4.setDescription("1 - Pelez, épépinez et coupez les pommes en morceaux. ...\n" +
                "2 - Dans un saladier, versez la farine, le beurre mou et la cassonade.\n" +
                "3 - Mélangez avec vos doigts de façon à obtenir une pâte sableuse.\n" +
                "4 - Recouvrez les pommes de pâte à crumble et enfourner th 6/7 (200°) pendant 35 minutes.\n" +
                "5 - Servez ce dessert avec une boule de glace vanille.");

        recipeRepository.save(recipe4);

        //This recipe is used only to test delete recipe
        Recipe recipe5 = new Recipe("Chocolat chaud");
        recipe5.setNbPeople(1);

        Component component51 = new Component();
        component51.setIngredient(ingredientRepository.findOne(12));
        component51.setQuantity(500);
        recipe5.add(component51);

        Component component52 = new Component();
        component52.setIngredient(ingredientRepository.findOne(3));
        component52.setQuantity(100);
        recipe5.add(component52);

        recipe5.setDescription("Mettez dans une casserole le lait et le chocolat. Faites chauffer jusqu'à ce que le chocolat soit fondu. Et miam !");
        recipeRepository.save(recipe5);

        Recipe recipe6 = new Recipe("Granita au Coca-Cola");
        recipe6.setNbPeople(2);
        recipe6.setDescription("\n" +
                "Étape 1 :\n" +
                "\n" +
                "Mélanger l'eau et le soda.\n" +
                "Étape 2 :\n" +
                "\n" +
                "Mettre le mélange dans une boite plastique.\n" +
                "Étape 3 :\n" +
                "\n" +
                "Toutes les 30 min casser la glace et arrêter quand l'ensemble semble convenable.\n");

        Component component61 = new Component();
        component61.setIngredient(ingredientRepository.findOne(14));
        component61.setQuantity(600);
        recipe6.add(component61);

        recipeRepository.save(recipe6);
    }

}

