package projet_course.domains.services;

import org.springframework.stereotype.Service;
import projet_course.config.security.SHA1;
import projet_course.domains.models.User;
import projet_course.repositories.UserRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> all(){
        return userRepository.findAll();
    }

    public User findById(int id){
        return userRepository.findOne(id);
    }

    public long create(String firstname, String lastname, String mail, String password){
        User user = new User(firstname, lastname, mail, password);

        if(!user.isValid())
            return -1;

        User u = findByMail(mail);

        if(u == null) {
            user.cryptPassword();
            user = userRepository.save(user);
            return user.getId();
        }else
            return -2;
    }

    private User findByMail(String mail){
        List<User> users = userRepository.findAll().stream().filter(u -> u.getMail().equals(mail)).collect(toList());
        if(users.size() == 1)
            return users.get(0);
        else
            return null;
    }

    public User login(String mail, String password) {
        List<User> users = userRepository.findAll().stream().filter(u -> u.getMail().equals(mail) && u.getPassword().equals(SHA1.crypt(password))).collect(toList());
        if(users.size() == 1)
            return users.get(0);
        else
            return null;
    }
}
