package projet_course.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projet_course.config.exceptions.InvalidParametersException;
import projet_course.config.exceptions.NotDeletedException;
import projet_course.config.exceptions.ResourceAlreadyExistsException;
import projet_course.config.exceptions.ResourceNotFoundException;
import projet_course.domains.models.Fridge;
import projet_course.domains.models.User;
import projet_course.domains.services.FridgeService;
import projet_course.domains.services.GoldenDataService;
import projet_course.domains.services.UserService;

@RestController
@RequestMapping(value = "/fridges")
public class FridgeController {
    private UserService userService;
    private FridgeService fridgeService;

    @Autowired()
    public FridgeController(UserService userService, FridgeService fridgeService, GoldenDataService goldenDataService){
        this.fridgeService = fridgeService;
        this.userService = userService;
        goldenDataService.initialize();
    }

    @RequestMapping(value = "/{iduser}", method = RequestMethod.GET)
    public Fridge findById(@PathVariable String iduser) {

        int id = Integer.parseInt(iduser);

        User user = userService.findById(id);
        if(user==null)
            throw new ResourceNotFoundException();

        return user.getFridge();

    }

    @RequestMapping(method = RequestMethod.POST, value = "/{iduser}")
    @ResponseStatus(HttpStatus.CREATED)
    public int addComponent(@RequestParam("ingredient_name") String ingredientName, @RequestParam("quantity") String quantityString, @PathVariable("iduser") String idUserString) {
        double quantity = Double.parseDouble(quantityString);
        int iduser = Integer.parseInt(idUserString);

        int result = fridgeService.addComponent(ingredientName, quantity, iduser);
        if(result == -1)
        {
            throw new ResourceAlreadyExistsException();
        }else if(result == -2){
            throw new ResourceNotFoundException();
        }else if(result == -3){
            throw new InvalidParametersException();
        }else
            return result;

    }

    @RequestMapping(method= RequestMethod.PUT, value = "/{iduser}")
    public int updateQuantity(@RequestParam("quantity") double quantity, @RequestParam("idcomponent") String idComponentString, @PathVariable("iduser") String iduser){
        int idUser = Integer.parseInt(iduser);
        int idComponent = Integer.parseInt(idComponentString);
        int result = fridgeService.updateQuantity(idUser,idComponent,quantity);

        if(result == -1)
            throw new ResourceNotFoundException();
        else if(result == -2){
            throw new InvalidParametersException();
        }
        else if(result == -3) {
            throw new NotDeletedException();
        }
        return idComponent;
    }

    @RequestMapping(method= RequestMethod.DELETE, value = "/{iduser}")
    public int deleteComponent(@RequestParam("idcomponent") String idcomponentString, @PathVariable("iduser") String iduser){
        int idUser = Integer.parseInt(iduser);
        int idcomponent = Integer.parseInt(idcomponentString);
        boolean result = fridgeService.deleteComponent(idUser,idcomponent);
        if(!result)
            throw new InvalidParametersException();

        return idcomponent;
    }

}
