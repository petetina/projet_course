package projet_course.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projet_course.config.exceptions.InvalidParametersException;
import projet_course.config.exceptions.ResourceAlreadyExistsException;
import projet_course.config.exceptions.ResourceNotFoundException;
import projet_course.domains.models.User;
import projet_course.domains.services.GoldenDataService;
import projet_course.domains.services.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    private UserService userService;

    @Autowired()
    public UserController(UserService userService, GoldenDataService goldenDataService){
        this.userService = userService;
        goldenDataService.initialize();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<User> findAll() {
        return userService.all();
    }

    @RequestMapping(value="/login",method = RequestMethod.GET)
    public User login(@RequestParam("mail") String mail, @RequestParam("password") String password) {

        User user = userService.login(mail,password);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        return user;

    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public long createUser(@RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname, @RequestParam("mail") String mail, @RequestParam("password") String password) {

        long id = userService.create(firstname, lastname, mail, password);

        if (id == -1) {
            throw new InvalidParametersException();
        }else if(id == -2){
            throw new ResourceAlreadyExistsException();
        }

        return id;

    }
}
