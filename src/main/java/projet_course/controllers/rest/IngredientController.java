package projet_course.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projet_course.config.exceptions.InvalidParametersException;
import projet_course.config.exceptions.NotCreatedException;
import projet_course.config.exceptions.NotDeletedException;
import projet_course.config.exceptions.ResourceNotFoundException;
import projet_course.domains.models.Ingredient;
import projet_course.domains.services.GoldenDataService;
import projet_course.domains.services.IngredientService;
import projet_course.domains.services.UnitService;

import java.util.List;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    private final IngredientService ingredientService;
    private final UnitService unitService;

    @Autowired()
    public IngredientController(IngredientService ingredientService, UnitService unitService, GoldenDataService goldenDataService) {
        this.ingredientService = ingredientService;
        this.unitService = unitService;
        goldenDataService.initialize();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Ingredient> findAll() {
        return ingredientService.all();
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public List<Ingredient> findAllByName(@PathVariable String name) {

        return ingredientService.findAllByName(name);
    }
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public int createIngredient(@RequestParam("name") String name, @RequestParam("unit") String unitName) {

        int id=-1;
        if(ingredientService.isNameValid(name)) {

            if (ingredientService.findByName(name) != null || !unitService.exists(unitName)) {
                throw new InvalidParametersException();
            }

            id = ingredientService.create(name, unitName);
            if(id == -1)
                throw new NotCreatedException();

        }else
            throw new InvalidParametersException();
        return id;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean delete(@PathVariable String id) {
        int idingredient = Integer.parseInt(id);

        if (!ingredientService.exists(idingredient)) {
            throw new ResourceNotFoundException();
        }

        boolean deleted = ingredientService.deleteById(idingredient);
        if(!deleted)
            throw new NotDeletedException();

        return deleted;

    }



}

