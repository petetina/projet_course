package projet_course.config;

import org.h2.server.web.WebServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;

public class WebInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.setServletContext(servletContext);
        ctx.register(WebConfig.class);
        ctx.register(SecurityConfig.class);
        ctx.register(JPAConfig.class);
        ctx.register(CrossOriginConfiguration.class);

        Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
        servlet.addMapping("/");
        servlet.setLoadOnStartup(1);

        //Pour h2 console
        ServletRegistration.Dynamic servletH2 = servletContext
                .addServlet("h2-console", new WebServlet());
        servletH2.setLoadOnStartup(2);
        servletH2.addMapping("/console/*");



    }

}